﻿using AutoMapper;


namespace WebApplication2
{
    public class ItemMapping :Profile
    {
        public ItemMapping()
        {
            CreateMap<Item, ItemDto>().ReverseMap();
        }
    }
}
