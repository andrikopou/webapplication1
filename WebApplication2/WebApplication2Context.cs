﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2
{
    public class WebApplication2Context :DbContext
    {
        public WebApplication2Context(DbContextOptions<WebApplication2Context> options)
            : base(options)
        {
        }

        public DbSet<Item> Item { get; set; }
    }


    public class DesignItemsDbContextFactory : IDesignTimeDbContextFactory<WebApplication2Context>
    {
        public WebApplication2Context CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var builder = new DbContextOptionsBuilder<WebApplication2Context>();
            var connectionString = "Server=(localdb)\\mssqllocaldb;Database=WebApplication1Context-2;Trusted_Connection=True;MultipleActiveResultSets=true";
            builder.UseSqlServer(connectionString);
            return new WebApplication2Context(builder.Options);
        }
    }
}
