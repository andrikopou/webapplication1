﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2
{
    public class Item
    {
        [Key]
        [Required]
        public long Id { get; set; }

        [Required]
        public string description { get; set; }

    }
}
