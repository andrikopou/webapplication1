﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Dtos;

namespace WebApplication1.Attributes
{
    public class PasswordValidatorAttribute : ValidationAttribute
    {
        public int minChar { get; set; }

        public int lowercase { get; set; }
        
        public int uppercase { get; set; }

        public PasswordValidatorAttribute(int minChar, int lowercase, int uppercase)
        { 
            this.minChar = minChar;
            this.lowercase = lowercase;
            this.uppercase = uppercase;
        }

        protected override ValidationResult IsValid(object pass,ValidationContext validationContext)
        {
            string password = pass as string;
            if (password.Length >= minChar && password.Any(char.IsUpper) && password.Any(char.IsLower) && password.Any(char.IsDigit))
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult("Password must have at least " + minChar + " characters, with numbers, uppercase and lowercase letters");
            }
        }
    }
}
