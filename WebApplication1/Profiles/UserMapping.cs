﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Dtos;
using WebApplication1.models;

namespace WebApplication1.Services
{
    public class UserMapping : Profile
    {
        public UserMapping()
        {
            CreateMap<UserDto, User>()
               .ForMember(dest => dest.customer, opt => opt.Ignore()).ReverseMap()
               .ForMember(dest => dest.customer, opt => opt.MapFrom(src => (!src.customer.Equals(null))? src.customer.Id : 0)) ; //.MapFrom(src => src.customer.Id));

        }


    }
    

}

