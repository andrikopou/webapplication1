﻿using AutoMapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Dtos;
using WebApplication1.models;

namespace WebApplication1.Services
{
    public class CustomerMapping : Profile
    {

        public CustomerMapping()
        {
            CreateMap<CustomerDto, Customer>()
                .ForMember(dest => dest.orders, opt => opt.Ignore()).ReverseMap()
                .ForMember(dest => dest.orders, opt => opt.MapFrom(src => src.orders.Select(s => s.Id)));
        }
    }
}
