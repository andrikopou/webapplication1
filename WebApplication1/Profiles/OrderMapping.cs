﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Dtos;
using WebApplication1.models;

namespace WebApplication1.Services
{
    public class OrderMapping :Profile
    {
        public OrderMapping()
        {
            
            CreateMap<OrderDto, Order>()
            .ForMember(dest => dest.customerId, opt => opt.MapFrom(src => src.customer))
            .ForMember(dest => dest.customer, opt => opt.Ignore())
            .ReverseMap()
            .ForMember(dest => dest.customer, opt => opt.MapFrom(src => src.customer.Id));

            CreateMap<UpdateOrder, Order>()
                .ReverseMap();
        }

       
    }
}
