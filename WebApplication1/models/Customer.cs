﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.models
{
    public class Customer : BaseEntity
    {
        [Key]
        [Required]
        public long Id { get; set; }

        [Required]
        public string userEmail { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        [ForeignKey("UserForeignKey")]
        public long userId { get; set; }


        public virtual ICollection<Order> orders { get; set; }
    }
}
