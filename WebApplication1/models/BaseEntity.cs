﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.models
{
    public class BaseEntity
    {
        [Key]
        [Required]
        public long Id { get; set; }
        
        //public string Email { get; set; }
        
        //public string password { get; set; }
    }
}
