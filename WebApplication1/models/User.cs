﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.models
{
    public class User : BaseEntity
    {
        [Key]
        [Required]
        public long Id { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string password { get; set; }

        public Customer customer { get; set; }
    }
}
