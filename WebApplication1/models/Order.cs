﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.models
{
    public class Order : BaseEntity
    {
        [Key]
        [Required]
        public long Id { get; set; }

        [Required]
        public string description { get; set; }

        [Required]
        public int quantity { get; set; }

        [Required]
        public DateTime time { get; set; }

        [Required]
        public string Country { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        public long postal_code { get; set; }

        [Required]
        public string address { get; set; }

        [Required]
        public long customerId { get; set; } 

        public virtual Customer customer { get; set; }

        public OrderDetails OrderDetails { get; set; } = OrderDetails.Empty;
    }
}
