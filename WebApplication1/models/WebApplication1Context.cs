﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using WebApplication1.Repos;

namespace WebApplication1.models
{
    public class WebApplication1Context: DbContext
    {

        public WebApplication1Context(DbContextOptions<WebApplication1Context> options)
            : base(options)
        {
        }

        public DbSet<User> User { get; set; }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<Order> Order { get; set; }



        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            OnBeforeSaving();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            OnBeforeSaving();
            return await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private void OnBeforeSaving()
        {
            var entries = ChangeTracker.Entries();
            
            foreach (var entry in entries)
            {
                
                if (entry.Entity is ITrackable trackable)
                {
                    var now = DateTime.Now;
                    var user = GetCurrentUser();

                    switch (entry.State)
                    {

                        case EntityState.Modified:
                            trackable.LastUpdatedAt = now;
                            trackable.LastUpdatedBy = user;
                            
                            break;

                        case EntityState.Added:
                            trackable.CreatedAt = now;
                            trackable.CreatedBy = user;
                            trackable.LastUpdatedAt = now;
                            trackable.LastUpdatedBy = user;

                            break;

                    }
                }
            }
        }

        private string GetCurrentUser()
        {
            return "UserName";
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Write Fluent API configurations here

            //Property Configurations

            modelBuilder.Entity<Order>()
                .HasOne<Customer>(c => c.customer)
                .WithMany(o => o.orders)
                .HasForeignKey(o => o.customerId)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();

            modelBuilder.Entity<Order>()
                .Property(o => o.Id)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<Order>()
                .HasKey(o => o.Id);

            modelBuilder.Entity<Order>()
                .OwnsOne(o => o.OrderDetails)
                .Property(o => o.CreatedAt);

            modelBuilder.Entity<Order>()
                .OwnsOne(o => o.OrderDetails)
                .Property(o => o.CreatedBy);

            modelBuilder.Entity<Order>()
                .OwnsOne(o => o.OrderDetails)
                .Property(o => o.LastUpdatedAt);

            modelBuilder.Entity<Order>()
                .OwnsOne(o => o.OrderDetails)
                .Property(o => o.LastUpdatedBy);

            modelBuilder.Entity<Order>()
                .Property(o => o.description)
                .IsRequired();


        }

    }
}
