﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ReflectionIT.Mvc.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Dtos;
using WebApplication1.IServices;
using WebApplication1.models;
using WebApplication1.Repos;
using WebApplication1.Validators;

namespace WebApplication1.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IConfigurationProvider _configurationProvider;


        public CustomerService(IMapper mapper, IConfigurationProvider configurationProvider, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _configurationProvider = configurationProvider;
            _unitOfWork = unitOfWork;
        }


        public async Task<bool> AddAsync(CustomerDto customerDto)
        {

            if (!await _unitOfWork.CustomerRepository.IfExist(c => c.userEmail == customerDto.userEmail || c.userId == customerDto.userId)
                && await _unitOfWork.UserRepository.IfExist(u => u.Id == customerDto.userId && u.Email == customerDto.userEmail))
            //!await _context.Customer.AnyAsync(c => c.userEmail == customerDto.userEmail || c.userId == customerDto.userId)
            //&& await _context.User.AnyAsync(u => u.Id == customerDto.userId && u.Email == customerDto.userEmail)
            // var c = new CustomerValidator();
            // var cu = c.Validate(customerDto);
            //if (cu.IsValid)
            {
                var customer = _mapper.Map<CustomerDto, Customer>(customerDto);
                await _unitOfWork.CustomerRepository.CreateAsync(customer);

                customerDto = _mapper.Map<Customer, CustomerDto>(customer);
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<PageInfo<CustomerDto>> GetAllAsync(int page, int itemsInPage)
        {
            //.Join(_context.User,customer => customer.userId,user => user.Id,(customer,user) => new { user,customer})
            //.Select(c => c.user)
            var customers = _unitOfWork.CustomerRepository.FindAllAsync();
            var NumberOfCustomers = await customers.CountAsync();

            var DtoCustomers = await customers.OrderBy(c => c.Id).Skip(itemsInPage * (page - 1))
                .Take(itemsInPage).ProjectTo<CustomerDto>(_configurationProvider).ToListAsync();

            int pages = (int) Math.Ceiling(NumberOfCustomers / (double)itemsInPage);

            PageInfo<CustomerDto> pageInfo = new PageInfo<CustomerDto>()
            {
                Items = DtoCustomers,
                NumberOfPages = pages,
                Page = page,
                NumberOfItems = customers.Count(),
                NumberOfItemsPerPage = itemsInPage
            };
            //var model = await PagingList.CreateAsync(DtoCustomers, itemsInPage, pages);

            return pageInfo;
        }

        public async Task<CustomerDto> GetCurrentAsync(long id)
        {
            var customer = await _unitOfWork.CustomerRepository.FindByIdAsync(id).FirstOrDefaultAsync();
            var current_customer = _mapper.Map<Customer, CustomerDto>(customer);
            return current_customer;
        }

        public async Task<bool> DeleteAsync(long id)
        {
            var customer = await _unitOfWork.CustomerRepository.FindByIdAsync(id).FirstOrDefaultAsync();
            if(customer!= null)
            {
                await _unitOfWork.CustomerRepository.DeleteAsync(customer);
                return true;
            }else
            {
                return false;
            }

        }

        public async Task<PageInfo<CustomerDto>> SearchByNameAsync(string name, int page, int itemsInPage)
        {
            List<CustomerDto> result = new List<CustomerDto>();
            var customers = _unitOfWork.CustomerRepository.FindAllAsync();
            var NumberOfCustomers = await customers.CountAsync();
            if(string.IsNullOrEmpty(name))
            {
                result = await customers.Skip(itemsInPage * (page - 1)).Take(itemsInPage)
                .ProjectTo<CustomerDto>(_configurationProvider).ToListAsync();
            }
            else
            {
                customers = customers.Where(c => c.Surname.Contains(name));
                result = await customers.Skip(itemsInPage * (page - 1)).Take(itemsInPage)
                .ProjectTo<CustomerDto>(_configurationProvider).ToListAsync();
            }
            int pages = (int) Math.Ceiling(NumberOfCustomers / (double)itemsInPage);
            PageInfo<CustomerDto> pageInfo = new PageInfo<CustomerDto>()
            {
                Items = result,
                NumberOfPages = pages,
                Page = page,
                NumberOfItems = customers.Count(),
                NumberOfItemsPerPage = itemsInPage
            };

            return pageInfo;

        }

        public async Task<bool> UpdateAsync(UpdateCustomer updateCustomer)
        {
            var customer = await _unitOfWork.CustomerRepository.Find(u => u.Id == updateCustomer.Id && u.userEmail == updateCustomer.userEmail).FirstOrDefaultAsync();
            if (customer != null)
            {
                customer.Name = updateCustomer.Name;
                customer.Surname = updateCustomer.Surname;
                customer.userEmail = updateCustomer.userEmail;
                await _unitOfWork.CustomerRepository.UpdateAsync(customer);
                return true;
            }
            else
                return false;
        }
    }
}
