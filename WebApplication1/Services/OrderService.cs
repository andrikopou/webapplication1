﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ReflectionIT.Mvc.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Dtos;
using WebApplication1.IServices;
using WebApplication1.models;
using WebApplication1.Repos;

namespace WebApplication1.Services
{
    public class OrderService : IOrderService
    {

        private readonly IUnitOfWork _unitOfWork; 
        private readonly IMapper _mapper;
        private readonly IConfigurationProvider _configurationProvider;

        public OrderService(IMapper mapper, IConfigurationProvider configurationProvider, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _configurationProvider = configurationProvider;
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> AddAsync(OrderDto orderDto)
        {
            var order = _mapper.Map<OrderDto, Order>(orderDto);
            await _unitOfWork.OrderRepository.CreateAsync(order);
            return true;
        }

        public async Task<OrderDto> GetCurrentAsync(long id)
        {
            var order =  _unitOfWork.OrderRepository.FindByIdAsync(id);
            var current_order = await order.ProjectTo<OrderDto>(_configurationProvider).FirstOrDefaultAsync();
            return current_order;
            
        }

        public async Task<PageInfo<OrderDto>> GetAllAsync(int page, int itemsInPage)
        {
            var orders = _unitOfWork.OrderRepository.FindAllAsync();
            var DtoOrders = await orders.OrderBy(o => o.Id).Skip(itemsInPage * (page - 1))
                .Take(itemsInPage).ProjectTo<OrderDto>(_configurationProvider).ToListAsync();

            var NumberOfOrders = await orders.CountAsync();
            int pages = (int) Math.Ceiling(NumberOfOrders / (double)itemsInPage);

            PageInfo<OrderDto> pageInfo = new PageInfo<OrderDto>()
            {
                Items = DtoOrders,
                NumberOfPages = pages,
                Page = page,
                NumberOfItems = orders.Count(),
                NumberOfItemsPerPage = itemsInPage
            };

            //var model = await PagingList.CreateAsync(AllOrders, itemsInPage, pages);
            if (pageInfo.NumberOfPages < 1) { pageInfo.NumberOfPages = 1; }

            return pageInfo;
        }

        public async Task<bool> DeleteAsync(long id)
        {
            var order = await _unitOfWork.OrderRepository.FindByIdAsync(id).FirstOrDefaultAsync();
            if (order != null)
            {
                await _unitOfWork.OrderRepository.DeleteAsync(order);
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> UpdateAsync(UpdateOrder updateOrder)
        {
            var order = await _unitOfWork.OrderRepository.Find(o => o.Id == updateOrder.Id).FirstOrDefaultAsync();

            if (order != null)
            {
                order.description = updateOrder.description;
                order.address = updateOrder.address;
                order.City = updateOrder.City;
                order.Country = updateOrder.Country;
                order.postal_code = updateOrder.postal_code;
                order.quantity = updateOrder.quantity;
                order.time = updateOrder.time;

                _unitOfWork.UpdateAsync(order.OrderDetails);
                await _unitOfWork.OrderRepository.UpdateAsync(order);

                return true;
            }
            else
                return false;
        }
    }
}
