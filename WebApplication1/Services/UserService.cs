﻿using AutoMapper;

using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using ReflectionIT.Mvc.Paging;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WebApplication1.Dtos;
using WebApplication1.IServices;
using WebApplication1.models;
using WebApplication1.Repos;
using IConfigurationProvider = AutoMapper.IConfigurationProvider;

namespace WebApplication1.Services
{
    public class UserService: IUserService
    {
        private readonly IMapper _mapper;
        private readonly IConfigurationProvider _iconfigurationprovider;
        private readonly IConfiguration _config;
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IMapper mapper, IConfigurationProvider configurationProvider, IConfiguration config, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _iconfigurationprovider = configurationProvider;
            _config = config;
            _unitOfWork = unitOfWork;
        }

        public string GenerateJSONWebToken(UserDto user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[] {
                new Claim(JwtRegisteredClaimNames.UniqueName, user.Email),
                //new Claim("DateOfJoing", userInfo.DateOfJoing.ToString("yyyy-MM-dd")),
                //new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };
            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              claims: claims,
              expires: DateTime.Now.AddMinutes(120),
              signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public async Task<UserDto> Authenticate(string username, string password)
        {
            var user = await Task.Run(() => _unitOfWork.UserRepository.Find(u => u.Email == username && u.password == password)
            .ProjectTo<UserDto>(_iconfigurationprovider)
            .FirstOrDefaultAsync()); 
            //_context.User.ProjectTo<UserDto>(_iconfigurationprovider).SingleOrDefault(x => x.Email == username && x.password == password));

            // return null if user not found
            if (user == null)
                return null;

            // authentication successful so return user details without password
            user.password = null;
           
            return user;
        }


        public async Task<bool> AddAsync(UserDto userDto)
        {

             if (!await _unitOfWork.UserRepository.IfExist(u => u.Email == userDto.Email))
             {
                var user = _mapper.Map<UserDto, User>(userDto);
                await _unitOfWork.UserRepository.CreateAsync(user);
                //await _unitOfWork.Commit();
                //await _context.User.AddAsync(user);
                //await _context.SaveChangesAsync();
                
                return true;
            }
            else
            { 
                return false;
            }
        }

        public async Task<PageInfo<UserDto>> GetAllAsync(int page, int itemsInPage)
        {
            var users = _unitOfWork.UserRepository.FindAllAsync();
            var DtoUsers = await users.OrderBy(u => u.Id).Skip(itemsInPage * (page - 1)).Take(itemsInPage).ProjectTo<UserDto>(_iconfigurationprovider).ToListAsync();
            var NumberOfUsers = await users.CountAsync();
            int pages = (int) Math.Ceiling(NumberOfUsers / (double)itemsInPage);
            PageInfo<UserDto> pageInfo = new PageInfo<UserDto>() { 
                Items = DtoUsers,
                NumberOfPages = pages,
                Page = page,
                NumberOfItems = users.Count(),
                NumberOfItemsPerPage = itemsInPage
            };
            //var model = await PagingList.CreateAsync(DtoUsers, itemsInPage, pages);

            return pageInfo;
        }

        public async Task<UserDto> GetCurrentAsync(long id)
        {
            var user = _unitOfWork.UserRepository.FindByIdAsync(id);
            var current_user = await user.ProjectTo<UserDto>(_iconfigurationprovider).FirstOrDefaultAsync();
            return current_user;
        }

        public async Task<bool> UpdateAsync(UpdateUser updateUser)
        {
            var user = await _unitOfWork.UserRepository.Find(u => u.Id == updateUser.Id && u.password == updateUser.password).FirstOrDefaultAsync();
            if (user != null)
            {
                user.password = updateUser.password;
                user.Email = updateUser.Email;
                await _unitOfWork.UserRepository.UpdateAsync(user);

                return true;
                //var customer = await _unitOfWork.CustomerRepository.Find(c => c.userId == UpdateUser.Id).FirstOrDefaultAsync();
                //if (customer != null)
                //{
                //    customer.Name = UpdateUser.Name;
                //    customer.Surname = UpdateUser.Surname;
                //    customer.userId = UpdateUser.Id;
                //    await _unitOfWork.CustomerRepository.UpdateAsync(customer);

                //    return true;
                //}
                //else
                //    return false;
            }
            else
                return false;
        }

        public string GetUsername()
        {
            throw new NotImplementedException();
        }
    }
}
