﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using FluentValidation;
using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using ReflectionIT.Mvc.Paging;
using System;
using System.Text;
using WebApplication1.Commands;
using WebApplication1.Dtos;
using WebApplication1.Handlers;
using WebApplication1.IServices;
using WebApplication1.Middlewares;
using WebApplication1.models;
using WebApplication1.Modules;
using WebApplication1.Services;
using WebApplication1.Validators;

namespace WebApplication1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {

            services.AddPaging();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = Configuration["Jwt:Issuer"],
                    ValidAudience = Configuration["Jwt:Issuer"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
                };
            });

            //services.AddMediatR();
            //services.AddScoped(typeof(IRequestHandler<>), typeof(CreateHandler<>));
            //services.AddTransient(typeof(IRequestHandler<,>), typeof(CreateHandler<>));


            services.AddDbContext<WebApplication1Context>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("WebApplication1Context")));


            services.AddMvc(setup => { }).AddFluentValidation().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);


            var containerBuilder = new ContainerBuilder();
            containerBuilder.Populate(services);
            containerBuilder.RegisterModule(new ApplicationModule());
            var container = containerBuilder.Build();
            return new AutofacServiceProvider(container);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseExceptionHandlingMiddleware();
            }
            else
            {
                app.UseExceptionHandlingMiddleware();
                app.UseExceptionHandler();
            }

            app.UseAuthentication();

            app.UseMvc();
        }
    }
}
