﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;
using WebApplication1.Commands;
using WebApplication1.Dtos;
using WebApplication1.IServices;
using WebApplication1.Repos;

namespace WebApplication1.Handlers
{
    public class SendEmailHandler : INotificationHandler<SendEmail>
    {
        private readonly ICustomerService _icustomerService;
        
        public SendEmailHandler(ICustomerService customerService)
        {
            _icustomerService = customerService;
        }
        public async Task Handle(SendEmail notification, CancellationToken cancellationToken)
        {
            var customer =  await _icustomerService.GetCurrentAsync(notification._order.customer);
            var smtpClient = new SmtpClient
            {
                Host = "smtp.gmail.com", // set your SMTP server name here
                Port = 587, // Port 
                EnableSsl = true,
                Credentials = new NetworkCredential("andrikopou@gmail.com", "doxaniforeika9")
            };

            using (var message = new MailMessage("andrikopou@gmail.com", customer.userEmail)
            {
                Subject = "API SUPPORT",
                Body = "Order Registration Successful!" + Environment.NewLine
                + "Order Details:" + Environment.NewLine
                + "Id: " + customer.orders.Last() + Environment.NewLine
                + "Description: " + notification._order.description + Environment.NewLine
                + "Quantity: " + notification._order.quantity + Environment.NewLine
                + "Country: " + notification._order.Country + Environment.NewLine
                + "City: " + notification._order.City + Environment.NewLine
                + "Postal Code: " + notification._order.postal_code + Environment.NewLine
                + "Address: " + notification._order.address + Environment.NewLine
                + "Customer Name: " + customer.Name + Environment.NewLine
                + "Customer Surname: " + customer.Surname
            })
            {
                await smtpClient.SendMailAsync(message);
            }

        }
    }
}
