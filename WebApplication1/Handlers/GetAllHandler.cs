﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApplication1.Commands;
using WebApplication1.Dtos;
using WebApplication1.IServices;

namespace WebApplication1.Handlers
{
    public class GetAllHandler<T> : IRequestHandler<GetAll<T>, IEnumerable<T>> where T : BaseDto, new()
    {
        private readonly IUserService _userService;
        private readonly ICustomerService _customerService;
        private readonly IOrderService _orderService;

        public GetAllHandler(IUserService userService, ICustomerService customerService, IOrderService orderService)
        {
            _userService = userService;
            _customerService = customerService;
            _orderService = orderService;
        }
        public async Task<IEnumerable<T>> Handle(GetAll<T> request, CancellationToken cancellationToken)
        {
            if (typeof(T) == typeof(CustomerDto))
            {
                dynamic customer = await _customerService.GetAllAsync(request._pages, request._itemsInPage);
                return customer;
            }
            else if (typeof(T) == typeof(UserDto))
            {
                dynamic user = await _userService.GetAllAsync(request._pages, request._itemsInPage);
                return user;
            }
            else
            {
                dynamic order = await _orderService.GetAllAsync(request._pages, request._itemsInPage);
                return order;
            }
        }
    }
}
