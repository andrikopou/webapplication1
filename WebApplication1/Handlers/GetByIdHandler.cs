﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApplication1.Commands;
using WebApplication1.Dtos;
using WebApplication1.IServices;

namespace WebApplication1.Handlers
{
    public class GetByIdHandler<T> : IRequestHandler<GetById<T>, T> where T : BaseDto, new()
    {
        private readonly IUserService _iuserService;
        private readonly ICustomerService _icustomerService;
        private readonly IOrderService _iorderService;
        public GetByIdHandler(IUserService userService, ICustomerService customerService, IOrderService orderService)
        {
            _iuserService = userService;
            _icustomerService = customerService;
            _iorderService = orderService;
        }
        public async Task<T> Handle(GetById<T> request, CancellationToken cancellationToken)
        {
            if (typeof(T) == typeof(CustomerDto))
            {
                dynamic customer = await _icustomerService.GetCurrentAsync(request._Id);
                return customer;
            }
            else if (typeof(T) == typeof(UserDto))
            {
                dynamic user = await _iuserService.GetCurrentAsync(request._Id);
                return user;
            }
            else
            {
                dynamic order = await _iorderService.GetCurrentAsync(request._Id);
                return order;
            }
        }
    }
}
