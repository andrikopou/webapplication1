﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApplication1.Commands;
using WebApplication1.Dtos;
using WebApplication1.IServices;

namespace WebApplication1.Handlers
{
    public class CreateHandler<T> : IRequestHandler<Create<T>,bool> where T : BaseDto, new()
    {
        private readonly IUserService _iuserService;
        private readonly ICustomerService _icustomerService;
        private readonly IOrderService _iorderService;
        public CreateHandler(IUserService userService, ICustomerService customerService, IOrderService orderService)
        {
            _iuserService = userService;
            _icustomerService = customerService;
            _iorderService = orderService;
        }

        public async Task<bool> Handle(Create<T> create, CancellationToken cancellationToken)
        {
            if (create.newDto.GetType() == typeof(UserDto))
            {
                var message = await _iuserService.AddAsync((UserDto)(object)create.newDto);
                return message;
            }
            else if (create.newDto.GetType() == typeof(CustomerDto))
            {
                var message = await _icustomerService.AddAsync((CustomerDto)(object)create.newDto);
                return message;
            }
            else
            {
                var message = await _iorderService.AddAsync((OrderDto)(object)create.newDto);
                return message;
            }
            
        }
    }
}
