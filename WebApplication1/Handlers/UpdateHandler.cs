﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApplication1.Commands;
using WebApplication1.Dtos;
using WebApplication1.IServices;

namespace WebApplication1.Handlers
{
    public class UpdateHandler<T> : IRequestHandler<Update<T>, bool> where T : BaseDto, new()
    {

        private readonly IUserService _userService;
        private readonly ICustomerService _customerService;
        private readonly IOrderService _orderService;

        public UpdateHandler(IUserService userService, ICustomerService customerService, IOrderService orderService)
        {
            _userService = userService;
            _customerService = customerService;
            _orderService = orderService;
        }

        public async Task<bool> Handle(Update<T> update, CancellationToken cancellationToken)
        {
            if (typeof(T) == typeof(UpdateCustomer))
            {
                var message = await _customerService.UpdateAsync((UpdateCustomer)(object)update.newDto);
                return message;
            }
            else if (typeof(T) == typeof(UpdateUser))
            {
                var message = await _userService.UpdateAsync((UpdateUser)(object)update.newDto);
                return message;
            }
            else
            {
                var message = await _orderService.UpdateAsync((UpdateOrder)(object)update.newDto);
                return message;
            }
        }
    }
}
