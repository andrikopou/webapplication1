﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Dtos;
using WebApplication1.models;

namespace WebApplication1.IServices
{
    public interface IUserService
    {
        Task<UserDto> GetCurrentAsync(long id);
        Task<bool> AddAsync(UserDto userDto);
        Task<PageInfo<UserDto>> GetAllAsync(int page, int itemsInPage);
        Task<UserDto> Authenticate(string username, string password);
        string GenerateJSONWebToken(UserDto user);
        Task<bool> UpdateAsync(UpdateUser updateUser);
        string GetUsername();

    }
}
