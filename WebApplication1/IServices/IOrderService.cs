﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Dtos;
using WebApplication1.models;

namespace WebApplication1.IServices
{
    public interface IOrderService
    {
        Task<bool> AddAsync(OrderDto orderDto);
        Task<bool> DeleteAsync(long id);
        Task<OrderDto> GetCurrentAsync(long id);
        Task<PageInfo<OrderDto>> GetAllAsync(int page, int itemsInPage);
        Task<bool> UpdateAsync(UpdateOrder updateOrder);
    }
}
