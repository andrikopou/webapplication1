﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Dtos;

namespace WebApplication1.IServices
{
    public interface ICustomerService
    {
        Task<bool> AddAsync(CustomerDto customerDto);
        Task<CustomerDto> GetCurrentAsync(long id);
        Task<PageInfo<CustomerDto>> GetAllAsync(int pages, int itemsInPage);
        Task<bool> DeleteAsync(long id);
        Task<PageInfo<CustomerDto>> SearchByNameAsync(string name, int page, int itemsInPage);
        Task<bool> UpdateAsync(UpdateCustomer updateCustomer);
    }
}
