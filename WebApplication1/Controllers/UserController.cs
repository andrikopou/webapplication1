﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ReflectionIT.Mvc.Paging;
using WebApplication1.Attributes;
using WebApplication1.Commands;
using WebApplication1.Dtos;
using WebApplication1.IServices;
using WebApplication1.models;

namespace WebApplication1.Controllers
{
    
    [Route("api/Users")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {

        private readonly IUserService _iuserService;
        private readonly IMediator _mediator;

        public UserController(IUserService iuserService, IMapper mapper, IMediator mediator)
        {
            _mediator = mediator;
            _iuserService = iuserService;
        }

        [Route("AllUsers")]
        [HttpGet]
        public async Task<ActionResult<PageInfo<UserDto>>> GetAllUsersAsync([FromQuery(Name = "page")]int page,[FromQuery(Name = "itemsInPage")] int itemsInPage)
        {

            //IEnumerable<UserDto> users = new UserDto[] { };
            var users = await _mediator.Send(new GetAll<UserDto>(page, itemsInPage));
            if(users == null)
                throw new ArgumentNullException();
            return Ok(users);
                             
        }

        
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate(UserDto user)
        {
            IActionResult response = Unauthorized();
            var AuthUser = await _iuserService.Authenticate(user.Email, user.password);

            if (AuthUser != null)
            {
                AuthUser.password = null;
                var token = _iuserService.GenerateJSONWebToken(AuthUser);
                response = Ok( new { Token = token, User = AuthUser } );
            }

            return response;
        }


        [HttpGet]
        [Route("currentUser/{id}")]
        public async Task<ActionResult<UserDto>> GetCurrentUserAsync(long id)
        {
            var current_user = await _mediator.Send(new GetById<UserDto>(id));

            if (current_user != null)
            {
                return Ok(current_user);
            }
            else
            {
                return BadRequest("User not exists!");
            }

        }

        
        [AllowAnonymous]
        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> RegisterAsync([FromBody] UserDto user)
        {

            //var message = await _iuserService.AddUserAsync(userDto);
            //var create = new Create<UserDto>(user);
            
            var message = await _mediator.Send(new Create<UserDto>(user));
            if (message)
            {
                user.password = null;
                var token = _iuserService.GenerateJSONWebToken(user);
                return Ok(new { Token = token, Registration = "successful!" } );
            }
            else
            {
                return BadRequest("User registration failed");
            }
        }
        
    
        
        // PUT api/values/5
        [HttpPut("UpdateUser")]
        public async Task<ActionResult> UpdateUserAsync([FromBody] UpdateUser updateUser)
        {
            var message = await _mediator.Send(new Update<UpdateUser>(updateUser));
            if (message)
                return Ok("User Update Successful!");
            return BadRequest("User update failed");
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void DeleteUser(int id)
        {
           
        }

    }
}
