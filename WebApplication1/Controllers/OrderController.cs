﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Commands;
using WebApplication1.Dtos;
using WebApplication1.IServices;
using WebApplication1.models;

namespace WebApplication1.Controllers
{
    [Authorize]
    [Route("api/Orders")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _iorderService;
        private readonly IMediator _mediator;

        public OrderController(WebApplication1Context context, IMapper mapper, IOrderService orderService, IMediator mediator)
        {
            _iorderService = orderService;
            _mediator = mediator;
        }


        // GET: api/Order
        [HttpGet]
        [Route("AllOrders")]
        public async Task<ActionResult<PageInfo<OrderDto>>> GetAllOrdersAsync([FromQuery(Name = "page")] int page,[FromQuery(Name = "itemsInPage")] int itemsInPage)
        {
            var orders = await _mediator.Send(new GetAll<OrderDto>(page, itemsInPage));
            if (orders == null)
                throw new ArgumentNullException();
            return Ok(orders);
        }

        // GET: api/Order/5
        [HttpGet("{id}", Name = "Get")]
        [Route("CurrentOrder/{id}")]
        public async Task<ActionResult<OrderDto>> GetCurrentOrderAsync(int id)
        {
            var order = await _mediator.Send(new GetById<OrderDto>(id));
            if(order == null)
            {
                return BadRequest("Order not exists!");
            }
            else
            {
                return Ok(order);
            }
        }

        // POST: api/Order
        [HttpPost]
        [Route("AddOrder")]
        public async Task<ActionResult> AddOrderAsync([FromBody] OrderDto order)
        {
            order.time = DateTime.Now;
            var message = await _mediator.Send(new Create<OrderDto>(order));
            if (message)
            {
                await _mediator.Publish(new SendEmail(order));
                return Ok("Order Registration Successful!");
            }
            else
            {
                return BadRequest("Order Registration failed");
            }
        }

        // PUT: api/Order/5
        [HttpPut("UpdateOrder")]
        public async Task<ActionResult> UpdateOrderAsync(int id, [FromBody] UpdateOrder updateOrder)
        {
            updateOrder.time = DateTime.Now;
            var message = await _mediator.Send(new Update<UpdateOrder>(updateOrder));
            if (message)
                return Ok("Order Update Successful!");
            return BadRequest("Order update failed");
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("DeleteOrder/{id}")]
        public async Task<IActionResult> DeleteOrder(long id)
        {
            var message = await _iorderService.DeleteAsync(id);
            if (message)
                return Ok("Order delete successful!");

            return BadRequest("Order not exists");
        }

    }
}
