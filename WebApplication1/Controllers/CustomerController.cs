﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Commands;
using WebApplication1.Dtos;
using WebApplication1.IServices;

namespace WebApplication1.Controllers
{
    [Authorize]
    [Route("api/Customers")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerService _icustomerService;
        private readonly IMediator _mediator;

        public CustomerController(ICustomerService icustomerService, IMediator mediator)
        {
            _icustomerService = icustomerService;
            _mediator = mediator;
        }

        // GET: api/Customer
        [HttpGet]
        [Route("AllCustomers")]
        public async Task<ActionResult<PageInfo<CustomerDto>>> GetAllCustomersAsync([FromQuery(Name = "page")]int page, [FromQuery(Name = "itemsInPage")] int itemsInPage)
        {
            var customers = await _mediator.Send(new GetAll<CustomerDto>(page, itemsInPage));
            if (customers == null)
                throw new ArgumentNullException();
            return Ok(customers);
        }

        // GET: api/Customer/5
        [HttpGet]
        [Route("CurrentCustomer/{id}")]
        public async Task<ActionResult<CustomerDto>> GetCurrentCustomerAsync(long id)
        {
            
            var customer = await _mediator.Send(new GetById<CustomerDto>(id));
            if (customer != null)
            {
                return Ok(customer);
            }
            else
            {
                return BadRequest("Customer not exists");
            }
        }

        // POST: api/Customer
        [HttpPost]
        [Route("AddCustomer")]
        public async Task<ActionResult<string>> AddCustomerAsync([FromBody] CustomerDto customer)
        {
            var Message = await _mediator.Send(new Create<CustomerDto>(customer));
            if (Message)
            {
                return Ok("Customer Registration Successful!");
            }
            else
            {
                return BadRequest("Customer Registration failed");
            }
        }

        // PUT: api/Customer/5
        [HttpPut("UpdateCustomer")]
        public async Task<ActionResult> UpdateCustomerAsync([FromBody] UpdateCustomer customer)
        {
            var message = await _mediator.Send(new Update<UpdateCustomer>(customer));
            if (message)
                return Ok("Customer Update Successful!");
            return BadRequest("Customer update failed");
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("DeleteCustomer/{id}")]
        public async Task<IActionResult> DeleteCustomerAsync(long id)
        {
            var message = await _icustomerService.DeleteAsync(id);
            if (message)
                return Ok("Customer delete successful!");

            return BadRequest("Customer not exists");
        }

        [HttpGet("SearchByName")]
        public async Task<PageInfo<CustomerDto>> SearchByNameAsync([FromBody]string name, [FromQuery(Name = "page")]int page, [FromQuery(Name = "itemsInPage")] int itemsInPage)
        {
            var customers = await _icustomerService.SearchByNameAsync(name, page, itemsInPage);
            return customers;
        }
    }
}
