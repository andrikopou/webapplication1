﻿using FluentValidation;
using FluentValidation.Validators;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApplication1.Dtos;
using WebApplication1.models;


namespace WebApplication1.Validators
{
    public class CustomerValidator: AbstractValidator<CustomerDto>
    {
        public CustomerValidator()
        {
            RuleFor(x => x.userEmail).NotNull().EmailAddress().NotEmpty();
            RuleFor(x => x.Name).NotNull().NotEmpty();
            RuleFor(x => x.Surname).NotNull().NotEmpty();
            RuleFor(x => x.userId).NotNull().NotEmpty();
        }
        
        

    }
}
