﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Dtos;

namespace WebApplication1.Commands
{
    public class SendEmail : INotification 
    {
        public OrderDto _order { get; set; }

        public SendEmail(OrderDto order)
        {
            _order = order;
        }
    }
}
