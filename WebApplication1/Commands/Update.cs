﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Dtos;

namespace WebApplication1.Commands
{
    public class Update<T> : IRequest<bool> where T : BaseDto, new()
    {
        public T newDto { get; set; }

        public Update(T newDto)
        {
            this.newDto = newDto;
        }
    }
}
