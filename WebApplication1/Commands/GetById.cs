﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Dtos;

namespace WebApplication1.Commands
{
    public class GetById<T> : IRequest<T> where T : BaseDto, new()
    {
        public long _Id { get; set; }

        public GetById(long Id)
        {
            _Id = Id;
        }
    }
}
