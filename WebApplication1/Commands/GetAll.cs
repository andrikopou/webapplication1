﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Dtos;

namespace WebApplication1.Commands
{
    public class GetAll<T> : IRequest<IEnumerable<T>> where T : BaseDto, new()  
    {
        public readonly int _pages;

        public readonly int _itemsInPage;

        public GetAll(int pages, int itemsInPage)
        {
            _pages = pages;
            _itemsInPage = itemsInPage;
        }
    }
}
