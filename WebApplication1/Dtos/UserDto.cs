﻿using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Attributes;
using WebApplication1.models;

namespace WebApplication1.Dtos
{
    
    public class UserDto : BaseDto
    {

        public long Id { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        [PasswordValidator(8, 1, 1)]
        public string password { get; set; }

        public long customer { get; set; }

        
    }
}
