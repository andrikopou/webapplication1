﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Attributes;

namespace WebApplication1.Dtos
{
    public class UpdateUser : BaseDto
    {
        [Required]
        public long Id { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        [PasswordValidator(8, 1, 1)]
        public string password { get; set; }

    }
}
