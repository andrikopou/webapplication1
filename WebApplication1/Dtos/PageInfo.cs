﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Dtos
{
    public class PageInfo<T> 
    {
        public ICollection<T> Items { get; set; }

        public int NumberOfPages { get; set; }

        public int Page { get; set; }

        public int NumberOfItems { get; set; }

        public int NumberOfItemsPerPage { get; set; }
    }
}
