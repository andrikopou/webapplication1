﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.models;

namespace WebApplication1.Dtos
{
    public class CustomerDto : BaseDto
    {
        
        public long Id { get; set; }

        [Required]
        public string userEmail { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        public long userId { get; set; }

        public ICollection<long> orders { get; set; }
        
    }
}
