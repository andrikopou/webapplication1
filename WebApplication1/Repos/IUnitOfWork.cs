﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.models;

namespace WebApplication1.Repos
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<User> UserRepository { get; }
        IRepository<Customer> CustomerRepository { get; }
        IRepository<Order> OrderRepository { get; }
        void UpdateAsync(BaseEntity entity);
        Task Commit();

    }
}
