﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WebApplication1.Dtos;
using WebApplication1.models;
using IConfigurationProvider = AutoMapper.IConfigurationProvider;

namespace WebApplication1.Repos
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly WebApplication1Context _context;
        private readonly IConfigurationProvider _iconfigurationprovider;
        private DbSet<T> entities;
        public Repository(WebApplication1Context context, IConfigurationProvider configurationProvider)
        {
            _context = context;
            entities = context.Set<T>();
            _iconfigurationprovider = configurationProvider;
        }


        public async Task CreateAsync(T entity)
        {
            if(entity == null)
              throw new ArgumentNullException("Invalid entity");
            await entities.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(T entity)
        {
            entities.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public IQueryable<T> Find(Expression<Func<T, bool>> filter = null)
        {
            return entities.Where(filter);
        }

        public  IQueryable<T> FindAllAsync()
        {
            var entity = entities;
            return entity;
        }

        public IQueryable<T> FindByIdAsync(long id)
        {
            var entity = entities.Where(e => e.Id == id);
            return entity;
        }

        public async Task<bool> IfExist(Expression<Func<T, bool>> filter = null)
        {
            return await entities.AnyAsync(filter);
        }

        public async Task UpdateAsync(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            //entities.Attach(entity);
            await _context.SaveChangesAsync();
        }

        //public IQueryable<T> FindByEmailAsync(string Email)
        //{
        //    var entity = entities.Where(e => e.Email == Email);
        //    return entity;
        //}

        //public IQueryable<T> Authenticate(string Email, string Password)
        //{
        //    var entity = entities.Where(e => e.Email == Email && e.password == Password);
        //    return entity;
        //}
    }
}
