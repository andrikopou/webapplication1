﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.models;

namespace WebApplication1.Repos
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly WebApplication1Context _context;
        private readonly IConfigurationProvider _configurationProvider;

        public IRepository<User> _userRepository;
        public IRepository<Customer> _customerRepository;
        public IRepository<Order> _orderRepository;

        public UnitOfWork(WebApplication1Context context, IConfigurationProvider configurationProvider)
        {
            _context = context;
            _configurationProvider = configurationProvider;
        }

        public IRepository<User> UserRepository
        {
            get
            {
                if (_userRepository == null)
                {
                    this._userRepository = new Repository<User>(_context, _configurationProvider);
                }
                return _userRepository;
            }
        }
        public IRepository<Customer> CustomerRepository {
            get
            {
                if (_customerRepository == null)
                {
                    this._customerRepository = new Repository<Customer>(_context, _configurationProvider);
                }
                return _customerRepository;
            }
        }

        public IRepository<Order> OrderRepository
        {
            get
            {
                if (_orderRepository == null)
                {
                    this._orderRepository = new Repository<Order>(_context, _configurationProvider);
                }
                return _orderRepository;
            }
        }

        public void UpdateAsync(BaseEntity entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }

        public async Task Commit()
        {
            await _context.SaveChangesAsync();
        }

        private bool isDisposed = false;
        public virtual void Dispose(bool disposing)
        {
            if(!isDisposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            isDisposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~UnitOfWork() {
            Dispose(false);
        }
    }
}
