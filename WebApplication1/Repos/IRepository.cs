﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WebApplication1.Dtos;
using WebApplication1.models;

namespace WebApplication1.Repos
{
    public interface IRepository<T> where T: class
    {
        IQueryable<T> FindAllAsync();
        IQueryable<T> FindByIdAsync(long id);
        Task CreateAsync(T entity);
        Task UpdateAsync(T entity);
        Task DeleteAsync(T entity);
        Task<bool> IfExist(Expression<Func<T, bool>> filter = null);
        IQueryable<T> Find(Expression<Func<T, bool>> filter = null);
        //IQueryable<T> FindByEmailAsync(string Email);
        //IQueryable<T> Authenticate(string Email, string Password);

    }
}
