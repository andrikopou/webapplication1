﻿using Autofac;
using AutoMapper;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Commands;
using WebApplication1.Dtos;
using WebApplication1.Handlers;
using WebApplication1.IServices;

using WebApplication1.Validators;
using WebApplication1.Services;
using WebApplication1.Repos;
using WebApplication1.models;

namespace WebApplication1.Modules
{
    public class ApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //builder.RegisterType<Mediator>().As<IMediator>().InstancePerLifetimeScope();
            //builder.RegisterType<CreateHandler<UserDto>>().AsImplementedInterfaces().InstancePerLifetimeScope();
            builder.RegisterType<Mediator>().As<IMediator>().InstancePerLifetimeScope();

            // request & notification handlers
            builder.Register<ServiceFactory>(context =>
            {
                var c = context.Resolve<IComponentContext>();
                return t => c.Resolve(t);
            });

            //builder.RegisterType<CreateHandler<UserDto>>().AsImplementedInterfaces().InstancePerDependency();
            //builder.RegisterType<CreateHandler<UserDto>>().As<IRequestHandler<Create<UserDto>, bool>>().InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(UpdateHandler<>)).As(typeof(IRequestHandler<,>)).InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(GetAllHandler<>)).As(typeof(IRequestHandler<,>)).InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(GetByIdHandler<>)).As(typeof(IRequestHandler<,>)).InstancePerLifetimeScope();
            builder.RegisterType(typeof(SendEmailHandler)).As(typeof(INotificationHandler<SendEmail>)).InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(CreateHandler<>)).As(typeof(IRequestHandler<,>)).InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(Repository<>)).As(typeof(IRepository<>)).InstancePerLifetimeScope();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();
            builder.RegisterType<UserService>().As<IUserService>().InstancePerLifetimeScope();
            builder.RegisterType<CustomerService>().As<ICustomerService>().InstancePerLifetimeScope();
            builder.RegisterType<OrderService>().As<IOrderService>().InstancePerLifetimeScope();


            builder.RegisterAssemblyTypes().AssignableTo(typeof(UserMapping)).As<Profile>();
            builder.RegisterAssemblyTypes().AssignableTo(typeof(CustomerMapping)).As<Profile>();
            builder.RegisterAssemblyTypes().AssignableTo(typeof(OrderMapping)).As<Profile>();
            builder.Register(c => new MapperConfiguration(cfg =>
            {
                //foreach (var profile in c.Resolve<IEnumerable<Profile>>())
                cfg.AddProfile(new UserMapping());
                cfg.AddProfile(new CustomerMapping());
                cfg.AddProfile(new OrderMapping());
                cfg.AllowNullDestinationValues = true;
                cfg.CreateMissingTypeMaps = false;

            })).AsSelf().As<IConfigurationProvider>().SingleInstance();

            builder.Register(c => c.Resolve<MapperConfiguration>().CreateMapper(c.Resolve)).As<IMapper>().InstancePerLifetimeScope();


            //builder.RegisterAssemblyTypes(typeof(IMediator).Assembly).AsSelf().AsImplementedInterfaces().InstancePerLifetimeScope();

            // builder.RegisterAssemblyTypes(typeof(IMediator).Assembly, typeof(IRequestHandler<RegisterUser, bool>).Assembly)
            //.AsClosedTypesOf(typeof(IRequestHandler<,>))
            //.AsImplementedInterfaces();

            builder.RegisterType<CustomerValidator>().As<AbstractValidator<CustomerDto>>().InstancePerLifetimeScope();
        }
    }
}
